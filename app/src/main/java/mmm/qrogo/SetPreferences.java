package mmm.qrogo;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

public class SetPreferences extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_preferences);
        Context context = this.getApplicationContext();

        DB db = new DB(context);
        // In your oncreate (or where ever you want to create your gridview)
        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new CategoriesList(context, db.getUnselectedCategories(), this));
    }
}
