package mmm.qrogo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

public class Category {
    private int id;
    private String name;
    private String type;

    public Category(int id, String name, String type){
        this.id = id;
        this.name = name;
        this.type = type;
    }

    // Getters and Setters
    public int getID() {
        return this.id;
    }
    public void setID(int id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getImage(){
        return this.type;
    }
    public void setImage(String image){
        this.type = image;
    }

    // Convert from byte array to bitmap
    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }
}