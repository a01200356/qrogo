package mmm.qrogo;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class DB extends SQLiteOpenHelper {


    private static final int DB_VERSION = 3;
    private static final String DB_NAME = "qro_go.db";
    private static final String CREATE_CATEGORIES_TABLE = "" +
            "CREATE TABLE categories " +
            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, type TEXT)";
    private static final String CREATE_PREFERENCES_TABLE = "" +
            "CREATE TABLE preferences " +
            "(user_id INTEGER, category_id INTEGER, FOREIGN KEY(category_id) REFERENCES categories(_id)," +
            " FOREIGN KEY(user_id) REFERENCES users(_id) )";
    private static final String CREATE_USER_TABLE = "" +
            "CREATE TABLE users " +
            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, password TEXT, image BLOB)";
    private static final String ENABLE_FOREIGN_KEY = "PRAGMA foreign_keys = 1";

    private static final String[] CATEGORIES = {
            "Museos", "Panadería", "Bares", "Librería", "Cafes", "Restaurantes"
    };
    private static final String[] TYPES = {
            "art_gallery", "bakery", "bar", "book_store", "cafe", "restaurant"
    };

    private Resources res;

    public DB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        res = context.getResources();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CATEGORIES_TABLE);
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(ENABLE_FOREIGN_KEY);
        db.execSQL(CREATE_PREFERENCES_TABLE);
        try {
            seed(db);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS categories");
        db.execSQL("DROP TABLE IF EXISTS users");
        db.execSQL("DROP TABLE IF EXISTS preferences");
        onCreate(db);
    }

    // Our Methods

    // Convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    private void seed(SQLiteDatabase db) throws NoSuchFieldException {
        for(int i = 0; i < CATEGORIES.length; i++){
            ContentValues values = new ContentValues();
            values.put("name", CATEGORIES[i]);
            values.put("type", TYPES[i]);
            db.insert("categories", null, values);
        }
        ContentValues values = new ContentValues();
        values.put("username", "Local User");
        values.put("password", "password");
        //values.put("image", "");
        db.insert("users", null, values);
    }

    public long insertCategory(String name, String type) throws SQLiteException{
        SQLiteDatabase db = getWritableDatabase();
        long new_id = 0;
        if (db != null) {
            ContentValues values = new ContentValues();
            values.put("name", name);
            values.put("type", type);
            new_id = db.insert("categories", null, values);
            db.close();
        }
        return new_id;
    }

    //Retrieves all categorieas
    public List<Category> getCategories() {
        SQLiteDatabase db = getReadableDatabase();

        List<Category> categories_list = new ArrayList<Category>();

        String[] selected_columns = {"_id", "name", "type"};
        Cursor c = db.query("categories", selected_columns, null, null, null, null, null, null);
        c.moveToFirst();
        do {
            Category cat = new Category(c.getInt(0), c.getString(1), c.getString(2));
            categories_list.add(cat);
        } while (c.moveToNext());
        db.close();
        c.close();
        return categories_list;
    }

    public void addPreference(int user_id, int category_id){
        SQLiteDatabase db = getReadableDatabase();
        db.execSQL("INSERT INTO preferences (user_id, category_id) VALUES (" + user_id + ", " + category_id + ")");
        db.close();
    }

    public List<Category> getUnselectedCategories(){ // La idea es que reciba el id del usuario que lo usa
        SQLiteDatabase db = getReadableDatabase();

        List<Category> categories_list = new ArrayList<Category>();

        String query = "SELECT categories._id, categories.name, categories.type" +
                " FROM categories" +
                " WHERE categories._id NOT IN (" +
                " SELECT preferences.category_id FROM preferences" +
                " WHERE user_id = ? )";

        Cursor c = db.rawQuery(query, new String[] {"1"}); // Hardcodeada

        c.moveToFirst();
        do {
            Category cat = new Category(c.getInt(0), c.getString(1), c.getString(2));
            categories_list.add(cat);
        } while (c.moveToNext());
        db.close();
        c.close();
        return categories_list;
    }

    void clearPreferences(){
        SQLiteDatabase db = getReadableDatabase();
        db.execSQL("DELETE FROM preferences WHERE user_id = 1");
        db.close();
    }
}
