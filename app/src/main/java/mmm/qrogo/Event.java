package mmm.qrogo;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class Event {
    private LatLng location;
    private String name;
    private String description;
    private Date date;

    public Event(LatLng location, String name, String description) {
        this.location = location;
        this.name = name;
        this.description = description;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
