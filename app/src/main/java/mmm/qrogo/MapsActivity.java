package mmm.qrogo;

import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        DB db = new DB(this.getApplicationContext());
        db.clearPreferences();
        Intent i = new Intent(MapsActivity.this, SetPreferences.class);
        startActivity(i);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Try to get user's location and move camera there.
        LatLng myLocation = this.getLocation();
        if (myLocation == null) {
            System.out.println("Coordenadas hardcodeadas porque el GPS se chinga a su puta madre.");
            myLocation = new LatLng(20.61287, -100.40371);
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15));

        Event events[] = {
                new Event(new LatLng(20.612075,-100.4025667), "Peda in my house", "Migue's house"),
                new Event(new LatLng(20.612111,-100.4022223), "Event 2", "Here"),
                new Event(new LatLng(20.6129272,-100.4031765), "Library", "Fun"),
                new Event(new LatLng(20.6152698,-100.404365), "OFERTAS EN COPPEL", "*pague en abonos chiquitos"),
                new Event(new LatLng(20.6188688,-100.3976646), "Erick VS Brandon in Querétaro 2000", "The most Epic fight of ALL TIME!"),
                new Event(new LatLng(20.61529,-100.3975994), "La Ingrata", "Es un antro por ai, cero..."),
                new Event(new LatLng(20.6144737,-100.3979907), "Unidad Cristiana", "Pray for Jesus"),
                new Event(new LatLng(20.6127417,-100.4006533), "Free food today", "All you can eat ONLY TODAY!!!1")
        };

        for (Event event : events) {
            mMap.addMarker(new MarkerOptions().position(event.getLocation()).title(event.getName()).snippet(event.getDescription()));
        }
    }

    // I DON'T KNOW IF THIS SHIT EVEN WORKS BECAUSE THE MOTHER FUCKING EMULATOR FUCKING SUX
    // TODO: Test with real phone and fix accordingly.
    public LatLng getLocation() {
        // Get the location manager
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, false);
        Location location;

        if (bestProvider == null) {
            return null;
        }

        try {
            location = locationManager.getLastKnownLocation(bestProvider);
        } catch (SecurityException e) {
            e.printStackTrace();
            return null;
        }
        Double lat,lon;
        try {
            lat = location.getLatitude ();
            lon = location.getLongitude ();
            return new LatLng(lat, lon);
        }
        catch (NullPointerException e){
            e.printStackTrace();
            return null;
        }
    }
}
