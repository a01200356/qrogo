package mmm.qrogo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.util.List;

public class CategoriesList extends BaseAdapter {
    private Context mContext;
    private Activity mActivity;
    private List<Category> filenames;

    // Gets the context so it can be used later
    public CategoriesList(Context c, List<Category> filenames, Activity a) {
        mContext = c;
        this.filenames = filenames;
        this.mActivity = a;
    }

    // Total number of things contained within the adapter
    public int getCount() {
        return filenames.size();
    }

    // Require for structure, not really used in my code.
    public Object getItem(int position) {
        return null;
    }

    // Require for structure, not really used in my code. Can
    // be used to get the id of an item in the adapter for
    // manual control.
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position,
                        View convertView, ViewGroup parent) {
        Button btn;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            btn = new Button(mContext);
            btn.setLayoutParams(new GridView.LayoutParams(300, 100));
            btn.setPadding(3, 3, 3, 3);
        }
        else {
            btn = (Button) convertView;
        }
        Category cat = filenames.get(position);
        btn.setText(cat.getName());
        // filenames is an array of strings
        btn.setTextColor(Color.WHITE);
        btn.setBackgroundResource(R.color.colorPrimary);
        btn.setId(position);

        // Set the onclicklistener so that pressing the button fires an event
        // We will need to implement this onclicklistner.
        btn.setOnClickListener(new MyOnClickListener(position, cat.getID(), mContext, mActivity));
        return btn;
    }
}

class MyOnClickListener implements View.OnClickListener {
    private final int position;
    private int id;
    private Context mContext;
    private Activity a;

    public MyOnClickListener(int position, int id, Context c, Activity a) {
        this.position = position;
        this.id = id;
        this.mContext = c;
        this.a = a;
    }

    public void onClick(View v) {
        // Preform a function based on the position
        DB db = new DB(mContext);
        db.addPreference(1, id); // User hardcodeado porque solo es local por ahora
        a.finish();
        Intent i = new Intent(mContext.getApplicationContext(), SetPreferences.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        a.startActivity(i);
    }
}
